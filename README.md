
# Paligo Spellchecker

This spellchecker uses [aspell](https://github.com/WojciechMula/aspell-python) and 
[hunspell](https://github.com/MSeal/cython_hunspell) python wrappers. 
See Dockerfile for list of available languages.
It also uses [fastapi](https://fastapi.tiangolo.com/deployment/docker/) for it's server structure.

## Build

### Build image

`docker build -t paligo-spellcheck -f Dockerfile .`

### Run image locally

`docker run -it -p 7000:7000 paligo-spellcheck`

## Deploy to [ECR](https://docs.aws.amazon.com/AmazonECR/latest/userguide/docker-push-ecr-image.html)

Make sure your aws config matches your target aws account.
Or create your aws config with `aws configure`. 
Example below with account:*Paligo-Testing* and region:*eu-west-1*.

1. Authenticate Docker client with ECR registry:
    `aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 592776778641.dkr.ecr.eu-west-1.amazonaws.com`

2. Tag local image:
    `docker tag paligo-spellcheck 592776778641.dkr.ecr.eu-west-1.amazonaws.com/spellcheck`

3. Push image to ECR:
    `docker push 592776778641.dkr.ecr.eu-west-1.amazonaws.com/spellcheck`



