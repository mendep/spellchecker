FROM public.ecr.aws/docker/library/python:3.9-slim

# install aspell dependencies
RUN apt update && apt install -y gcc libaspell-dev
 
# install aspell and aspell languages
RUN apt install -y aspell \
aspell-ar \
aspell-bg \
aspell-ca \
aspell-cs \
aspell-da \
aspell-de \
aspell-el \
aspell-es \
aspell-et \
aspell-eu \
aspell-fa \
aspell-fr \
aspell-he \
aspell-hi \
aspell-hr \
aspell-hsb \
aspell-hu \
aspell-it \
aspell-lt \
aspell-lv \
aspell-nl \
aspell-no \
aspell-pl \
aspell-pt \
aspell-pt-br \
aspell-pt-pt \
aspell-ro \
aspell-ru \
aspell-sk \
aspell-sl \
aspell-sv \
aspell-ta \
aspell-uk

# install hunspell and hunspell languages
RUN apt install -y hunspell \
hunspell-af \
#hunspell-ar \
hunspell-be \
#hunspell-bg \
hunspell-bs \
#hunspell-ca \
#hunspell-cs \
#hunspell-da \
#hunspell-de-de \
#hunspell-el \
hunspell-en-au \
hunspell-en-ca \
hunspell-en-gb \
hunspell-en-us \
hunspell-en-za \
#hunspell-es \
#hunspell-eu \
#hunspell-fr \
#hunspell-he \
#hunspell-hr \
#hunspell-hu \
#hunspell-id \
#hunspell-is \
#hunspell-it \
hunspell-ko \
#hunspell-lo \
#hunspell-lt \
#hunspell-lv \
#hunspell-nl \
#hunspell-no \
#hunspell-pl \
#hunspell-pt-br \
#hunspell-pt-pt \
#hunspell-ro \
#hunspell-ru \
#hunspell-sk \
hunspell-sr \
#hunspell-sv \
hunspell-th \
hunspell-tr \
#hunspell-uk \
hunspell-uz \
hunspell-vi

# copy all files 
COPY . .

# install python dependencies
RUN pip install -r requirements.txt

# Change the working directory 
WORKDIR /app

# expose server port 
EXPOSE 7000

# start server
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "7000"]
