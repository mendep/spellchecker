# Spellchecker class
import os
import nltk
import string
import subprocess
import re
from fastapi import HTTPException

import aspell
from hunspell import Hunspell

try:
    nltk.data.find('tokenizers/punkt')
except LookupError:
    nltk.download('punkt')

hunspell_dict_path = '/usr/share/hunspell'
hunspell_cache_dir = '/app/cache'

class Spellchecker:
    def __init__(self):
        self.hunspeller = Hunspeller()
        self.aspeller = Aspeller()
        # add both lists(aspell + hunspell), convert to set to remove duplicates, convert to list and sort it
        self.supported_languages = sorted(list(set(self.hunspeller.supported_languages + self.aspeller.supported_languages)))

    # returns supported commands
    def help(self):
        return {"commands": ['/languages [GET]', '/check [POST: lang (string), text (string)]']}

    # returns supported languages
    def get_supported_languages(self):
        return self.supported_languages
        
    # check some text
    def check(self, lang, text):
        if lang in self.hunspeller.supported_languages:
            return {'result': self.hunspeller.spell(lang, text), 'engine': 'hunspell'}

        if lang in self.aspeller.supported_languages:
            return {'result': self.aspeller.spell(lang, text), 'engine': 'aspell'}

        raise HTTPException(status_code=404, detail="Language it not supported")

# spellchecker implementation with hunspell
# this class returns supported languages by
# hunspell and does spellchecking with hunspell
class Hunspeller:
    # initialise the class
    def __init__(self) -> None:
        self.supported_languages = self.get_supported_languages()

    # returns supported languages by this class
    def get_supported_languages(self):
        global hunspell_dict_path
        languages = []
        files = os.listdir(hunspell_dict_path)

        for filename in files:
            if filename[-4:] == '.aff':
                languages.append(filename[:-4])

        return languages

    # returns an object with misspelled words
    def spell(self, lang, text):
        hunspell_speller = Hunspell(lang, disk_cache_dir=hunspell_cache_dir, hunspell_data_dir=hunspell_dict_path)
        suggest = []

        words = nltk.word_tokenize(text)
        words = list(set(words))

        for word in words:
            word = word.strip(string.punctuation)

            if not hunspell_speller.spell(word):
                suggest.append(word)

        if suggest:
            suggest = hunspell_speller.bulk_suggest(suggest)

        return suggest

class Aspeller:
    def __init__(self) -> None:
        self.supported_languages = self.get_supported_languages()

    # returns supported languages by aspell
    def get_supported_languages(self):
        languages = []

        process_result = subprocess.run(["aspell", "dicts"], capture_output=True)
        aspell_dicts = process_result.stdout.decode()

        # aspell will dump some esoteric languages. those
        # languages are filtered out here
        p = re.compile('^[a-z]{2}(_[a-z]{2})?$', re.IGNORECASE)

        for language in aspell_dicts.split("\n"):
            if p.match(language):
                languages.append(language)

        return languages

    # returns an object with misspelled words
    def spell(self, lang, text):
        aspell_speller = aspell.Speller('lang', lang)
        suggest = {}

        words = nltk.word_tokenize(text)
        words = list(set(words))

        for word in words:
            word = word.strip(string.punctuation)

            if not aspell_speller.check(word):
                suggest[word] = tuple(aspell_speller.suggest(word))

        return suggest
