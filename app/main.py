from fastapi import FastAPI, Form
from spellchecker import Spellchecker

app = FastAPI()

spellchecker = Spellchecker()

@app.get("/")
def read_root():
    return spellchecker.help()

@app.post("/check/")
async def check_text(lang: str = Form(...), text: str = Form(...)):
    return spellchecker.check(lang, text)

@app.get("/languages")
def supported_languages():
    return spellchecker.get_supported_languages()
